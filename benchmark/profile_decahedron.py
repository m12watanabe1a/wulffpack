from wulffpack import Decahedron  # NOQA
import cProfile

surface_energies = {(1, 1, 1): 1.0,
                    (1, 0, 0): 1.1,
                    (1, 1, 0): 1.1,
                    (2, 1, 1): 1.1,
                    (3, 2, 1): 1.1,
                    (5, 1, 1): 1.1,
                    (5, 4, 0): 1.1,
                    (5, 3, 0): 1.1,
                    (5, 2, 0): 1.1,
                    (5, 1, 0): 1.1,
                    }
twin_energy = 0.05

cProfile.run('Decahedron(surface_energies, twin_energy=twin_energy)', sort='cumtime')

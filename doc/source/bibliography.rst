.. _bibliography:
.. index:: Bibliography

Bibliography
************

.. [HowMar84]
   | A. Howie and L. D. Marks
   | *Elastic strains and the energy balance for multiply twinned particles*
   | Philosophical Magazine A **49**, 95 (1984)
   | `doi: 10.1080/01418618408233432`

.. [Mar83]
   | L. D. Marks
   | *Modified Wulff constructions for twinned particles*
   | J. Cryst. Growth **61**, 556 (1983)
   | `doi: 10.1016/0022-0248(83)90184-7 <https://doi.org/10.1016/0022-0248(83)90184-7>`_

.. [Win67]
   | W. L. Winterbottom
   | *Equilibrium shape of a small particle in contact with a foreign substrate*
   | Acta Metall. **15**, 303 (1967)
   | `doi: 10.1016/0001-6160(67)90206-4 <https://doi.org/10.1016/0001-6160(67)90206-4>`_

.. [Wul01]
   | G. Wulff
   | *XXV. Zur Frage der Geschwindigkeit des Wachsthums und der Auflösung der Krystallflächen*
   | Z. Krystallog. **34**, 449 (1901)
   | `doi: 10.1524/zkri.1901.34.1.449 <https://doi.org/10.1524/zkri.1901.34.1.449>`_

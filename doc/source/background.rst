Background
==========

The equilibrium shape of a droplet of water in a spaceship is a sphere. The
reason is that the sphere is the shape that has the smallest area for a given
volume, which means that a spherical water droplet minimizes its energy.

The situation is more complicated for crystalline materials, because the surface
energy is a function of direction in such materials. Given a direction dependent
surface energy :math:`\gamma[\boldsymbol{n}]`, the Wulff construction provides
the lowest energy shape [Wul01]_. The construction can be performed for each
direction :math:`\boldsymbol{n}` drawing a plane with :math:`\boldsymbol{n}` at
a distance  :math:`\gamma[\boldsymbol{n}]` from the origin, and then
constructing the inner envelope of those planes. In mathematical terms (and
slightly simplified), the construction is thus the set of points 

.. math::

      \mathcal{W} =  \left\lbrace \boldsymbol{x}:\, \boldsymbol{x} \cdot
      \boldsymbol{n} \leq \gamma\left[\boldsymbol{n}\right]\, \text{for all}\,
      \boldsymbol{n} \right\rbrace.

The only input needed to make a Wulff construction is thus the direction
dependent surface energy :math:`\gamma[\boldsymbol{n}]`. It must obey the
symmetry of the crystal, which significantly reduces the data needed. In
practice, a good approximation to :math:`\mathcal{W}` can usually be obtained by
considering only a small number of directions :math:`\boldsymbol{n}`
corresponding to low-index facets of the crystal. A Wulff construction can thus
often be made based on the input from only a very small number of energy
calculations, often based on density functional theory. In :program:`WulffPack`,
a Wulff construction is performed by creating a :class:`SingleCrystal
<wulffpack.SingleCrystal>` object.

Wulff constructions of decahedra and icosahedra
-----------------------------------------------

The regular Wulff construction described above assumes a defect-free single
crystal. It has been demonstrated many times that small nanoparticles made of
FCC metals but with twin boundaries often have a lower energy than single
crystalline nanoparticles with the same shape. Two particularly common such
particles are based on `decahedral
<https://en.wikipedia.org/wiki/Pentagonal_bipyramid>`_ and `icosahedral
<https://en.wikipedia.org/wiki/Icosahedron>`_ symmetry. They can be viewed as
consisting of five and twenty tetrahedral grains, respepctively. Fortunately, a
Wulff construction can be made for such particles as well [Mar83]_ (see
:class:`Decahedron <wulffpack.Decahedron>` and :class:`Icosahedron
<wulffpack.Icosahedron>`). :program:`WulffPack` also allows various aspects of
Wulff constructions of these shapes to be compared, see further under
:ref:`Compare shapes <examples_compare_shapes>`.

Particles on flat surfaces
--------------------------

When a particle is placed on a flat surface, symmetry is broken and the
equilibrium shapes is changed. A Wulff construction can still be made as long as
the broken symmetry is taken into account. Such a construction is usually
referred to as a Winterbottom construction [Win67]_. In :program:`WulffPack`,
this is handled with a :class:`Winterbottom <wulffpack.Winterbottom>` object.